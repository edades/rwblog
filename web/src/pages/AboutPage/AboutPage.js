import NavLayout from 'src/layouts/NavLayout/NavLayout'

const AboutPage = () => {
  return (
    <NavLayout>
      <h1>AboutPage</h1>
      <p>Find me in ./web/src/pages/AboutPage/AboutPage.js</p>
    </NavLayout>
  )
}

export default AboutPage
