import NavLayout from 'src/layouts/NavLayout/NavLayout'
import BlogPostsCell from 'src/components/BlogPostsCell/BlogPostsCell'

const HomePage = () => {
  return (
    <NavLayout>
      <BlogPostsCell />
    </NavLayout>
  )
}

export default HomePage
