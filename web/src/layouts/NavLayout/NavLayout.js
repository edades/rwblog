import { Link, routes } from '@redwoodjs/router'

const NavLayout = ({ children }) => {
  return (
    <>
      <nav>
        <Link to={routes.home()}>Home</Link>
        <Link to={routes.about()}>About</Link>
        <Link to={routes.newPost()}>New Post</Link>
      </nav>
      {children}
    </>
  )
}

export default NavLayout
